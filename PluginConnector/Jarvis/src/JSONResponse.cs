﻿using System;

namespace Jarvis {
	public enum JSONResponseType {
		PLUGIN_CONNECT = 0,
		PLUGIN_DISCONNECT = 1,
		PLUGIN_ACTION = 2,
	}

	public class JSONResponse<T> {
		public string name;
		public JSONResponseType type;
		public T data;

		public JSONResponse() { }
	}

	namespace ResponseType {
		public class Connect {
			public string name;
			public string description;
			public string version;
			public int socketId;
		}

		public class Disconnect {
			/* Nothing */
		}

		public class Action {
			public string Do;
			public string[] Params;
		}
	}
}

