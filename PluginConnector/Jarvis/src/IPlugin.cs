﻿using System;
using System.Collections.Generic;


namespace Jarvis {
	public interface IPlugin : IDisposable {
		string Name { get; set; }
		string Version { get; set; }
		string Description { get; set; }

		void Initialize();
		void OnMessage(string Request, Dictionary<string, object> Params);
		void OnClose();
	}
}

