﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Windows.Forms;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace Jarvis {
	public static class PluginConnector {
		public delegate void MessageHandler(string Request, Dictionary<string, object> Params);
		public delegate void RequestHandler(object[] P);
		public delegate void LogHandler(string message);

		private static TcpClient client { get; set; }
		private static NetworkStream stream { get; set; }
		private static MessageHandler onMessageHandler { get; set; }
		private static Dictionary<string, RequestHandler> requestHandlers { get; set; }

		public static LogHandler OnLog { get; set; }

		public static void Initialize() {
			requestHandlers = new Dictionary<string, RequestHandler>();

			do {
				try {
					client = new TcpClient("localhost", 5500);
					stream = client.GetStream();

					new Thread(() => {
						do {
							if (client.Available > 0) {
								byte[] B = new byte[client.Available];
								stream.Read(B, 0, B.Length);

								string buffer = System.Text.Encoding.Unicode.GetString(B);
								
								if(!buffer.EndsWith("$")) {
									PluginConnector.Log("PluginConnector: Unterminated buffer...");
									stream.Seek(-B.Length, SeekOrigin.Current);
									continue;
								}

								string[] data = buffer.Split("$".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

								foreach (string jsonObj in data) {
									JSONRequest R = JsonConvert.DeserializeObject<JSONRequest>(jsonObj);
									PluginConnector.Log("PluginConnector: Ricevuta richiesta " + R.Request);

									if (requestHandlers.ContainsKey(R.Request)) {
										List<object> Px = new List<object>();
										if (R.Params != null)
											foreach (string[] V in R.Params)
												Px.Add((object)V[1]);

										requestHandlers[R.Request].Invoke(Px.ToArray());
									}


									Dictionary<string, object> P = new Dictionary<string, object>();

									if (R.Params != null)
										foreach (string[] V in R.Params)
											P.Add(V[0], (object)V[1]);

									onMessageHandler.Invoke(R.Request, P);
								}
							}

							Thread.Sleep(50);
						} while (true);
					}).Start();

					PluginConnector.Log("PluginConnector: Connesso");
					break;
				} catch (Exception e) {
					PluginConnector.Log("PluginConnector: " + e.Message);
				}

				Application.DoEvents();
				Thread.Sleep(100);
			} while (true);
		}

		public static void Connect(IPlugin Plugin) {
			onMessageHandler = Plugin.OnMessage;
			Plugin.Initialize();

			JSONResponse<Jarvis.ResponseType.Connect> R = new JSONResponse<Jarvis.ResponseType.Connect>();
			R.name = Plugin.Name;
			R.type = JSONResponseType.PLUGIN_CONNECT;

			R.data = new ResponseType.Connect() {
				name = Plugin.Name,
				description = Plugin.Description,
				version = Plugin.Version,
				socketId = 0
			};

			string json = JsonConvert.SerializeObject(R);
			byte[] data = System.Text.Encoding.Unicode.GetBytes(json);

			stream.Write(data, 0, data.Length);
			PluginConnector.Log("PluginConnector: PLUGIN_CONNECT");
		}

		public static void Disconnect(IPlugin Plugin) {
			Plugin.OnClose();

			JSONResponse<Jarvis.ResponseType.Action> R = new JSONResponse<Jarvis.ResponseType.Action>();
			R.name = Plugin.Name;
			R.type = JSONResponseType.PLUGIN_DISCONNECT;
			R.data = null;

			JsonSerializerSettings S = new JsonSerializerSettings();

			string json = JsonConvert.SerializeObject(R);
			byte[] data = System.Text.Encoding.Unicode.GetBytes(json);

			stream.Write(data, 0, data.Length);
			PluginConnector.Log("PluginConnector: PLUGIN_DISCONNECT");
		}

		public static void Action(IPlugin Plugin, string action, params string[] p) {
			JSONResponse<Jarvis.ResponseType.Action> R = new JSONResponse<Jarvis.ResponseType.Action>();
			R.name = Plugin.Name;
			R.type = JSONResponseType.PLUGIN_ACTION;

			R.data = new ResponseType.Action() {
				Do = action,
				Params = p,
			};

			JsonSerializerSettings S = new JsonSerializerSettings();

			string json = JsonConvert.SerializeObject(R);
			byte[] data = System.Text.Encoding.Unicode.GetBytes(json);

			stream.Write(data, 0, data.Length);
			PluginConnector.Log("PluginConnector: PLUGIN_ACTION");
		}

		public static void Run() {
			Thread.CurrentThread.Join();
		}

		public static void AddRequestHandler(string message, RequestHandler e) {
			requestHandlers.Add(message, e);
		}

		public static void Log(string message) {
			if (OnLog != null)
				OnLog.Invoke(message);

			Console.WriteLine(message);
		}

	}
}

