﻿namespace HomeManager {
	partial class MainWindow {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
			this.pbPlain = new System.Windows.Forms.PictureBox();
			this.stripInfo = new System.Windows.Forms.StatusStrip();
			this.txtInfo = new System.Windows.Forms.ToolStripStatusLabel();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.tabControl2 = new System.Windows.Forms.TabControl();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.tabControl3 = new System.Windows.Forms.TabControl();
			this.tabPage3 = new System.Windows.Forms.TabPage();
			this.label7 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.tabControl4 = new System.Windows.Forms.TabControl();
			this.tabPage4 = new System.Windows.Forms.TabPage();
			this.label10 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.KitchenDoors = new HomeManager.SlideBox();
			this.KitchenHeat = new HomeManager.SlideBox();
			this.KitchenLights = new HomeManager.SlideBox();
			this.GarageDoors = new HomeManager.SlideBox();
			this.GarageHeat = new HomeManager.SlideBox();
			this.GarageLights = new HomeManager.SlideBox();
			this.LivingDoors = new HomeManager.SlideBox();
			this.LivingHeat = new HomeManager.SlideBox();
			this.LivingLights = new HomeManager.SlideBox();
			this.EntranceDoors = new HomeManager.SlideBox();
			this.EntranceHeat = new HomeManager.SlideBox();
			this.EntranceLights = new HomeManager.SlideBox();
			((System.ComponentModel.ISupportInitialize)(this.pbPlain)).BeginInit();
			this.stripInfo.SuspendLayout();
			this.tabControl1.SuspendLayout();
			this.tabPage1.SuspendLayout();
			this.tabControl2.SuspendLayout();
			this.tabPage2.SuspendLayout();
			this.tabControl3.SuspendLayout();
			this.tabPage3.SuspendLayout();
			this.tabControl4.SuspendLayout();
			this.tabPage4.SuspendLayout();
			this.SuspendLayout();
			// 
			// pbPlain
			// 
			this.pbPlain.Image = global::HomeManager.Properties.Resources.plain;
			this.pbPlain.Location = new System.Drawing.Point(0, -114);
			this.pbPlain.Name = "pbPlain";
			this.pbPlain.Size = new System.Drawing.Size(696, 660);
			this.pbPlain.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pbPlain.TabIndex = 0;
			this.pbPlain.TabStop = false;
			// 
			// stripInfo
			// 
			this.stripInfo.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.txtInfo});
			this.stripInfo.Location = new System.Drawing.Point(0, 491);
			this.stripInfo.Name = "stripInfo";
			this.stripInfo.Size = new System.Drawing.Size(952, 22);
			this.stripInfo.TabIndex = 1;
			this.stripInfo.Text = "statusStrip1";
			// 
			// txtInfo
			// 
			this.txtInfo.Name = "txtInfo";
			this.txtInfo.Size = new System.Drawing.Size(0, 17);
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.tabPage1);
			this.tabControl1.Location = new System.Drawing.Point(702, 12);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(238, 100);
			this.tabControl1.TabIndex = 2;
			// 
			// tabPage1
			// 
			this.tabPage1.Controls.Add(this.EntranceDoors);
			this.tabPage1.Controls.Add(this.EntranceHeat);
			this.tabPage1.Controls.Add(this.EntranceLights);
			this.tabPage1.Controls.Add(this.label3);
			this.tabPage1.Controls.Add(this.label2);
			this.tabPage1.Controls.Add(this.label1);
			this.tabPage1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
			this.tabPage1.Location = new System.Drawing.Point(4, 22);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage1.Size = new System.Drawing.Size(230, 74);
			this.tabPage1.TabIndex = 0;
			this.tabPage1.Text = "Ingresso";
			this.tabPage1.UseVisualStyleBackColor = true;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(6, 3);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(42, 20);
			this.label1.TabIndex = 0;
			this.label1.Text = "Luci:";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(6, 23);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(113, 20);
			this.label2.TabIndex = 1;
			this.label2.Text = "Riscaldamenti:";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(6, 43);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(71, 20);
			this.label3.TabIndex = 2;
			this.label3.Text = "Finestre:";
			// 
			// tabControl2
			// 
			this.tabControl2.Controls.Add(this.tabPage2);
			this.tabControl2.Location = new System.Drawing.Point(702, 137);
			this.tabControl2.Name = "tabControl2";
			this.tabControl2.SelectedIndex = 0;
			this.tabControl2.Size = new System.Drawing.Size(238, 100);
			this.tabControl2.TabIndex = 3;
			// 
			// tabPage2
			// 
			this.tabPage2.Controls.Add(this.LivingDoors);
			this.tabPage2.Controls.Add(this.LivingHeat);
			this.tabPage2.Controls.Add(this.LivingLights);
			this.tabPage2.Controls.Add(this.label4);
			this.tabPage2.Controls.Add(this.label5);
			this.tabPage2.Controls.Add(this.label6);
			this.tabPage2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
			this.tabPage2.Location = new System.Drawing.Point(4, 22);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage2.Size = new System.Drawing.Size(230, 74);
			this.tabPage2.TabIndex = 0;
			this.tabPage2.Text = "Soggiorno";
			this.tabPage2.UseVisualStyleBackColor = true;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(6, 43);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(71, 20);
			this.label4.TabIndex = 2;
			this.label4.Text = "Finestre:";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(6, 23);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(113, 20);
			this.label5.TabIndex = 1;
			this.label5.Text = "Riscaldamenti:";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(6, 3);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(42, 20);
			this.label6.TabIndex = 0;
			this.label6.Text = "Luci:";
			// 
			// tabControl3
			// 
			this.tabControl3.Controls.Add(this.tabPage3);
			this.tabControl3.Location = new System.Drawing.Point(702, 263);
			this.tabControl3.Name = "tabControl3";
			this.tabControl3.SelectedIndex = 0;
			this.tabControl3.Size = new System.Drawing.Size(238, 100);
			this.tabControl3.TabIndex = 4;
			// 
			// tabPage3
			// 
			this.tabPage3.Controls.Add(this.GarageDoors);
			this.tabPage3.Controls.Add(this.GarageHeat);
			this.tabPage3.Controls.Add(this.GarageLights);
			this.tabPage3.Controls.Add(this.label7);
			this.tabPage3.Controls.Add(this.label8);
			this.tabPage3.Controls.Add(this.label9);
			this.tabPage3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
			this.tabPage3.Location = new System.Drawing.Point(4, 22);
			this.tabPage3.Name = "tabPage3";
			this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage3.Size = new System.Drawing.Size(230, 74);
			this.tabPage3.TabIndex = 0;
			this.tabPage3.Text = "Garage";
			this.tabPage3.UseVisualStyleBackColor = true;
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(6, 43);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(71, 20);
			this.label7.TabIndex = 2;
			this.label7.Text = "Finestre:";
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(6, 23);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(113, 20);
			this.label8.TabIndex = 1;
			this.label8.Text = "Riscaldamenti:";
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(6, 3);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(42, 20);
			this.label9.TabIndex = 0;
			this.label9.Text = "Luci:";
			// 
			// tabControl4
			// 
			this.tabControl4.Controls.Add(this.tabPage4);
			this.tabControl4.Location = new System.Drawing.Point(702, 388);
			this.tabControl4.Name = "tabControl4";
			this.tabControl4.SelectedIndex = 0;
			this.tabControl4.Size = new System.Drawing.Size(238, 100);
			this.tabControl4.TabIndex = 5;
			// 
			// tabPage4
			// 
			this.tabPage4.Controls.Add(this.KitchenDoors);
			this.tabPage4.Controls.Add(this.KitchenHeat);
			this.tabPage4.Controls.Add(this.KitchenLights);
			this.tabPage4.Controls.Add(this.label10);
			this.tabPage4.Controls.Add(this.label11);
			this.tabPage4.Controls.Add(this.label12);
			this.tabPage4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
			this.tabPage4.Location = new System.Drawing.Point(4, 22);
			this.tabPage4.Name = "tabPage4";
			this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage4.Size = new System.Drawing.Size(230, 74);
			this.tabPage4.TabIndex = 0;
			this.tabPage4.Text = "Cucina";
			this.tabPage4.UseVisualStyleBackColor = true;
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(6, 43);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(71, 20);
			this.label10.TabIndex = 2;
			this.label10.Text = "Finestre:";
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Location = new System.Drawing.Point(6, 23);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(113, 20);
			this.label11.TabIndex = 1;
			this.label11.Text = "Riscaldamenti:";
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Location = new System.Drawing.Point(6, 3);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(42, 20);
			this.label12.TabIndex = 0;
			this.label12.Text = "Luci:";
			// 
			// KitchenDoors
			// 
			this.KitchenDoors.Checked = false;
			this.KitchenDoors.Location = new System.Drawing.Point(173, 43);
			this.KitchenDoors.Margin = new System.Windows.Forms.Padding(9, 12, 9, 12);
			this.KitchenDoors.Name = "KitchenDoors";
			this.KitchenDoors.ReadOnly = true;
			this.KitchenDoors.Size = new System.Drawing.Size(50, 20);
			this.KitchenDoors.TabIndex = 5;
			this.KitchenDoors.TextOFF = "OFF";
			this.KitchenDoors.TextON = "ON";
			// 
			// KitchenHeat
			// 
			this.KitchenHeat.Checked = false;
			this.KitchenHeat.Location = new System.Drawing.Point(173, 23);
			this.KitchenHeat.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
			this.KitchenHeat.Name = "KitchenHeat";
			this.KitchenHeat.ReadOnly = true;
			this.KitchenHeat.Size = new System.Drawing.Size(50, 20);
			this.KitchenHeat.TabIndex = 4;
			this.KitchenHeat.TextOFF = "OFF";
			this.KitchenHeat.TextON = "ON";
			// 
			// KitchenLights
			// 
			this.KitchenLights.Checked = false;
			this.KitchenLights.Location = new System.Drawing.Point(173, 3);
			this.KitchenLights.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.KitchenLights.Name = "KitchenLights";
			this.KitchenLights.ReadOnly = true;
			this.KitchenLights.Size = new System.Drawing.Size(50, 20);
			this.KitchenLights.TabIndex = 3;
			this.KitchenLights.TextOFF = "OFF";
			this.KitchenLights.TextON = "ON";
			// 
			// GarageDoors
			// 
			this.GarageDoors.Checked = false;
			this.GarageDoors.Location = new System.Drawing.Point(173, 43);
			this.GarageDoors.Margin = new System.Windows.Forms.Padding(9, 12, 9, 12);
			this.GarageDoors.Name = "GarageDoors";
			this.GarageDoors.ReadOnly = true;
			this.GarageDoors.Size = new System.Drawing.Size(50, 20);
			this.GarageDoors.TabIndex = 5;
			this.GarageDoors.TextOFF = "OFF";
			this.GarageDoors.TextON = "ON";
			// 
			// GarageHeat
			// 
			this.GarageHeat.Checked = false;
			this.GarageHeat.Location = new System.Drawing.Point(173, 23);
			this.GarageHeat.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
			this.GarageHeat.Name = "GarageHeat";
			this.GarageHeat.ReadOnly = true;
			this.GarageHeat.Size = new System.Drawing.Size(50, 20);
			this.GarageHeat.TabIndex = 4;
			this.GarageHeat.TextOFF = "OFF";
			this.GarageHeat.TextON = "ON";
			// 
			// GarageLights
			// 
			this.GarageLights.Checked = false;
			this.GarageLights.Location = new System.Drawing.Point(173, 3);
			this.GarageLights.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.GarageLights.Name = "GarageLights";
			this.GarageLights.ReadOnly = true;
			this.GarageLights.Size = new System.Drawing.Size(50, 20);
			this.GarageLights.TabIndex = 3;
			this.GarageLights.TextOFF = "OFF";
			this.GarageLights.TextON = "ON";
			// 
			// LivingDoors
			// 
			this.LivingDoors.Checked = false;
			this.LivingDoors.Location = new System.Drawing.Point(173, 43);
			this.LivingDoors.Margin = new System.Windows.Forms.Padding(9, 12, 9, 12);
			this.LivingDoors.Name = "LivingDoors";
			this.LivingDoors.ReadOnly = true;
			this.LivingDoors.Size = new System.Drawing.Size(50, 20);
			this.LivingDoors.TabIndex = 5;
			this.LivingDoors.TextOFF = "OFF";
			this.LivingDoors.TextON = "ON";
			// 
			// LivingHeat
			// 
			this.LivingHeat.Checked = false;
			this.LivingHeat.Location = new System.Drawing.Point(173, 23);
			this.LivingHeat.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
			this.LivingHeat.Name = "LivingHeat";
			this.LivingHeat.ReadOnly = true;
			this.LivingHeat.Size = new System.Drawing.Size(50, 20);
			this.LivingHeat.TabIndex = 4;
			this.LivingHeat.TextOFF = "OFF";
			this.LivingHeat.TextON = "ON";
			// 
			// LivingLights
			// 
			this.LivingLights.Checked = false;
			this.LivingLights.Location = new System.Drawing.Point(173, 3);
			this.LivingLights.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.LivingLights.Name = "LivingLights";
			this.LivingLights.ReadOnly = true;
			this.LivingLights.Size = new System.Drawing.Size(50, 20);
			this.LivingLights.TabIndex = 3;
			this.LivingLights.TextOFF = "OFF";
			this.LivingLights.TextON = "ON";
			// 
			// EntranceDoors
			// 
			this.EntranceDoors.Checked = false;
			this.EntranceDoors.Location = new System.Drawing.Point(173, 43);
			this.EntranceDoors.Margin = new System.Windows.Forms.Padding(9, 12, 9, 12);
			this.EntranceDoors.Name = "EntranceDoors";
			this.EntranceDoors.ReadOnly = true;
			this.EntranceDoors.Size = new System.Drawing.Size(50, 20);
			this.EntranceDoors.TabIndex = 5;
			this.EntranceDoors.TextOFF = "OFF";
			this.EntranceDoors.TextON = "ON";
			// 
			// EntranceHeat
			// 
			this.EntranceHeat.Checked = false;
			this.EntranceHeat.Location = new System.Drawing.Point(173, 23);
			this.EntranceHeat.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
			this.EntranceHeat.Name = "EntranceHeat";
			this.EntranceHeat.ReadOnly = true;
			this.EntranceHeat.Size = new System.Drawing.Size(50, 20);
			this.EntranceHeat.TabIndex = 4;
			this.EntranceHeat.TextOFF = "OFF";
			this.EntranceHeat.TextON = "ON";
			// 
			// EntranceLights
			// 
			this.EntranceLights.Checked = false;
			this.EntranceLights.Location = new System.Drawing.Point(173, 3);
			this.EntranceLights.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.EntranceLights.Name = "EntranceLights";
			this.EntranceLights.ReadOnly = true;
			this.EntranceLights.Size = new System.Drawing.Size(50, 20);
			this.EntranceLights.TabIndex = 3;
			this.EntranceLights.TextOFF = "OFF";
			this.EntranceLights.TextON = "ON";
			// 
			// MainWindow
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.ClientSize = new System.Drawing.Size(952, 513);
			this.Controls.Add(this.tabControl4);
			this.Controls.Add(this.tabControl3);
			this.Controls.Add(this.tabControl2);
			this.Controls.Add(this.tabControl1);
			this.Controls.Add(this.stripInfo);
			this.Controls.Add(this.pbPlain);
			this.DoubleBuffered = true;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.Name = "MainWindow";
			this.Text = "Home Manager";
			((System.ComponentModel.ISupportInitialize)(this.pbPlain)).EndInit();
			this.stripInfo.ResumeLayout(false);
			this.stripInfo.PerformLayout();
			this.tabControl1.ResumeLayout(false);
			this.tabPage1.ResumeLayout(false);
			this.tabPage1.PerformLayout();
			this.tabControl2.ResumeLayout(false);
			this.tabPage2.ResumeLayout(false);
			this.tabPage2.PerformLayout();
			this.tabControl3.ResumeLayout(false);
			this.tabPage3.ResumeLayout(false);
			this.tabPage3.PerformLayout();
			this.tabControl4.ResumeLayout(false);
			this.tabPage4.ResumeLayout(false);
			this.tabPage4.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.PictureBox pbPlain;
		private System.Windows.Forms.StatusStrip stripInfo;
		private System.Windows.Forms.ToolStripStatusLabel txtInfo;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage tabPage1;
		private SlideBox EntranceDoors;
		private SlideBox EntranceHeat;
		private SlideBox EntranceLights;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TabControl tabControl2;
		private System.Windows.Forms.TabPage tabPage2;
		private SlideBox LivingDoors;
		private SlideBox LivingHeat;
		private SlideBox LivingLights;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TabControl tabControl3;
		private System.Windows.Forms.TabPage tabPage3;
		private SlideBox GarageDoors;
		private SlideBox GarageHeat;
		private SlideBox GarageLights;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TabControl tabControl4;
		private System.Windows.Forms.TabPage tabPage4;
		private SlideBox KitchenDoors;
		private SlideBox KitchenHeat;
		private SlideBox KitchenLights;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label12;
	}
}