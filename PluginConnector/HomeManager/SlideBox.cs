﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HomeManager {
	public partial class SlideBox : UserControl {

		private bool _Checked = false;
		public bool Checked {
			set {
				_Checked = value;
				calcPoints();
				this.Invalidate();
			}
			get {
				return _Checked;
			}
		}

		public bool ReadOnly { get; set; }
		public string TextON { get; set; }
		public string TextOFF { get; set; }

		private Rectangle lineRect { get; set; }
		private Rectangle btnRect { get; set; }
		private RectangleF txtRect { get; set; }


		private void calcPoints() {
			this.lineRect = new Rectangle(0, this.Height / 2, this.Width, 1);

			if (!this._Checked)
				this.btnRect = new Rectangle(0, 0, (int)((float)this.Width / 1.5f), this.Height);
			else
				this.btnRect = new Rectangle(this.Width - (int)((float)this.Width / 1.5f), 0, (int)((float)this.Width / 1.5f), this.Height);

			this.txtRect = new RectangleF(this.btnRect.X + 5, this.btnRect.Y + 5, this.btnRect.Width - 5, this.btnRect.Height - 5);
		}

		public SlideBox() {
			InitializeComponent();

			this.Checked = false;
			this.ReadOnly = false;
			this.TextON = "ON";
			this.TextOFF = "OFF";
		}



		

		protected override void OnSizeChanged(EventArgs e) {
			calcPoints();
			base.OnSizeChanged(e);
		}

		protected override void OnClick(EventArgs e) {
			if(!this.ReadOnly)
				this.Checked = !this.Checked;
			this.Invalidate();
			base.OnClick(e);
		}

		protected override void OnPaint(PaintEventArgs e) {
			base.OnPaint(e);

			if(Checked) {
				e.Graphics.FillRectangle(Brushes.Gray, lineRect);
				e.Graphics.FillRectangle(Brushes.DarkGreen, btnRect);
				e.Graphics.DrawString(this.TextON, new Font("Segoe UI", 8f, FontStyle.Bold), Brushes.White, (RectangleF)txtRect);
			} else {
				e.Graphics.FillRectangle(Brushes.Gray, lineRect);
				e.Graphics.FillRectangle(Brushes.DarkRed, btnRect);
				e.Graphics.DrawString(this.TextOFF, new Font("Segoe UI", 8f, FontStyle.Bold), Brushes.White, (RectangleF)txtRect);
			}
		}
	}
}
