﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Threading;
using System.Net.Sockets;
using Jarvis;

namespace HomeManager {
	public partial class MainWindow : Form, Jarvis.IPlugin {
		public MainWindow() {
			InitializeComponent();
		}

		public new string Name { get; set; }
		public string Description { get; set; }
		public string Version { get; set; }


		public void Initialize() {
			this.Name = "Home";
			this.Description = "Plugin for Jarvis to control and manage house";
			this.Version = "0.0.0.1";

			PluginConnector.AddRequestHandler("LivingDoorsOpen", (e) => LivingDoors.Checked = true);
			PluginConnector.AddRequestHandler("LivingDoorsClose", (e) => LivingDoors.Checked = false);
			PluginConnector.AddRequestHandler("KitchenDoorsOpen", (e) => KitchenDoors.Checked = true);
			PluginConnector.AddRequestHandler("KitchenDoorsClose", (e) => KitchenDoors.Checked = false);
			PluginConnector.AddRequestHandler("EntranceDoorsOpen", (e) => EntranceDoors.Checked = true);
			PluginConnector.AddRequestHandler("EntranceDoorsClose", (e) => EntranceDoors.Checked = false);
			PluginConnector.AddRequestHandler("GarageDoorsOpen", (e) => GarageDoors.Checked = true);
			PluginConnector.AddRequestHandler("GarageDoorsClose", (e) => GarageDoors.Checked = false);
			PluginConnector.AddRequestHandler("AllDoorsOpen", (e) => {
				LivingDoors.Checked = true;
				KitchenDoors.Checked = true;
				EntranceDoors.Checked = true;
				GarageDoors.Checked = true;
			});
			PluginConnector.AddRequestHandler("AllDoorsClose", (e) => {
				LivingDoors.Checked = false;
				KitchenDoors.Checked = false;
				EntranceDoors.Checked = false;
				GarageDoors.Checked = false;
			});


			PluginConnector.AddRequestHandler("LivingLightsOpen", (e) => LivingLights.Checked = true);
			PluginConnector.AddRequestHandler("LivingLightsClose", (e) => LivingLights.Checked = false);
			PluginConnector.AddRequestHandler("KitchenLightsOpen", (e) => KitchenLights.Checked = true);
			PluginConnector.AddRequestHandler("KitchenLightsClose", (e) => KitchenLights.Checked = false);
			PluginConnector.AddRequestHandler("EntranceLightsOpen", (e) => EntranceLights.Checked = true);
			PluginConnector.AddRequestHandler("EntranceLightsClose", (e) => EntranceLights.Checked = false);
			PluginConnector.AddRequestHandler("GarageLightsOpen", (e) => GarageLights.Checked = true);
			PluginConnector.AddRequestHandler("GarageLightsClose", (e) => GarageLights.Checked = false);
			PluginConnector.AddRequestHandler("AllLightsOpen", (e) => {
				LivingLights.Checked = true;
				KitchenLights.Checked = true;
				EntranceLights.Checked = true;
				GarageLights.Checked = true;
			});
			PluginConnector.AddRequestHandler("AllLightsClose", (e) => {
				LivingLights.Checked = false;
				KitchenLights.Checked = false;
				EntranceLights.Checked = false;
				GarageLights.Checked = false;
			});

			PluginConnector.AddRequestHandler("LivingHeatOpen", (e) => { LivingHeat.Checked = true; LivingHeat.TextON = (e[0] as string) + "°"; });
			PluginConnector.AddRequestHandler("LivingHeatClose", (e) => LivingHeat.Checked = false);
			PluginConnector.AddRequestHandler("KitchenHeatOpen", (e) => { KitchenHeat.Checked = true; KitchenHeat.TextON = (e[0] as string) + "°"; });
			PluginConnector.AddRequestHandler("KitchenHeatClose", (e) => KitchenHeat.Checked = false);
			PluginConnector.AddRequestHandler("EntranceHeatOpen", (e) => { EntranceHeat.Checked = true; EntranceHeat.TextON = (e[0] as string) + "°"; });
			PluginConnector.AddRequestHandler("EntranceHeatClose", (e) => EntranceHeat.Checked = false);
			PluginConnector.AddRequestHandler("GarageHeatOpen", (e) => { GarageHeat.Checked = true; GarageHeat.TextON = (e[0] as string) + "°"; });
			PluginConnector.AddRequestHandler("GarageHeatClose", (e) => GarageHeat.Checked = false);
			PluginConnector.AddRequestHandler("AllHeatOpen", (e) => {
				LivingHeat.Checked = true; LivingHeat.TextON = (e[0] as string) + "°";
				KitchenHeat.Checked = true; KitchenHeat.TextON = (e[0] as string) + "°";
				EntranceHeat.Checked = true; EntranceHeat.TextON = (e[0] as string) + "°";
				GarageHeat.Checked = true; GarageHeat.TextON = (e[0] as string) + "°";
			});
			PluginConnector.AddRequestHandler("AllHeatClose", (e) => {
				LivingHeat.Checked = false;
				KitchenHeat.Checked = false;
				EntranceHeat.Checked = false;
				GarageHeat.Checked = false;
			});
		}

		public void OnClose() {
			
		}

		public void OnMessage(string Request, Dictionary<string, object> Params) {
			if (Stream != null) {
				var b = System.Text.Encoding.Unicode.GetBytes(Request);
				Stream.Write(b, 0, b.Length);

				PluginConnector.Log("UnityServer: Invio richiesta " + Request);
			}
		}


		TcpListener UnityServer;
		TcpClient UnityClient;
		NetworkStream Stream;

		protected override void OnLoad(EventArgs e) {
			base.OnLoad(e);

			txtInfo.Text = "In attesa di Jarvis...";
			Application.DoEvents();

			this.FormClosing += (s, ev) => { PluginConnector.Disconnect(this); System.Diagnostics.Process.GetCurrentProcess().Kill(); };
			PluginConnector.OnLog = (ev) => txtInfo.Text = ev;


			UnityServer = new TcpListener(5501);
			UnityServer.Start();

			new Thread(() => {
				for (; ; Thread.Sleep(50)) {
					if (!UnityServer.Pending())
						continue;

					UnityClient = UnityServer.AcceptTcpClient();
					Stream = UnityClient.GetStream();
					break;
				}
			}).Start();

			new Thread(() => {
				PluginConnector.Initialize();
				PluginConnector.Connect(this);
				PluginConnector.Run();
			}).Start();
		}
	}
}
