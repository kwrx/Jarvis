﻿using System;
using System.Collections.Generic;
using Jarvis;


public class TestPlugin : IPlugin {
	public string Description { get; set; }
	public string Name { get; set; }
	public string Version { get; set; }

	public void Dispose() {
		
	}

	public void Initialize() {
		this.Name = "TestPlugin";
		this.Description = "External plugin used for test";
		this.Version = "0.0.0.1";
	}

	public void OnClose() {
		
	}

	public void OnMessage(string Request, Dictionary<string, object> Params) {
		
	}
}

static class Program {
	static void Main(string[] args) {

		using (TestPlugin T = new TestPlugin()) {
			PluginConnector.Initialize();
			PluginConnector.Connect(T);
			PluginConnector.Run();
		}
	}
}