﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Jarvis;


public class MediaPlugin : IPlugin {
	public string Description { get; set; }
	public string Name { get; set; }
	public string Version { get; set; }

	WMPLib.WindowsMediaPlayer MusicPlayer { get; set; }
	List<string> MusicFiles { get; set; }
	string SelectedMusic { get; set; }
	string MusicPath { get; set; }


	void LoadMusic() {
		MusicPlayer = new WMPLib.WindowsMediaPlayer();
		MusicFiles = new List<string>();
		SelectedMusic = "";


		MusicPath = System.IO.File.ReadAllText("Media.conf");

		MusicFiles.AddRange(System.IO.Directory.GetFiles(MusicPath, "*.mp3", System.IO.SearchOption.AllDirectories));
		Console.WriteLine("Media: Found " + MusicFiles.Count + " songs");
	}

	public void Dispose() {

	}

	public void Initialize() {
		this.Name = "Media";
		this.Description = "External Plugin to manage files";
		this.Version = "0.0.0.1";

		LoadMusic();



		PluginConnector.AddRequestHandler("OpenApplication", (e) => {
			string app = e[0] as string;

			if (app == "Browser")
				Process.Start("www.google.it");
			else
				Process.Start(app);
		});

		PluginConnector.AddRequestHandler("PlayMusic", (e) => {
			string song = e[0] as string;
			string artist = e[1] as string;


			if (song == "undefined") {
				if (SelectedMusic == "")
					MusicPlayer.URL = SelectedMusic = MusicFiles[new Random().Next(0, MusicFiles.Count)];

				MusicPlayer.controls.play();

			} else {
				try {
					SelectedMusic = (from M in MusicFiles where M.ToLower().Contains(song.ToLower()) && (artist != "" ? M.ToLower().Contains(artist.ToLower()) : true) select M).ToArray()[0];

					if (SelectedMusic == "")
						SelectedMusic = MusicFiles[new Random().Next(0, MusicFiles.Count)];
				} catch (Exception ex) {
					PluginConnector.Action(this, "Log", "Media: " + ex.Message);
					PluginConnector.Action(this, "Talk", "Non ho trovato quel che cercavi");
				}

				MusicPlayer.URL = SelectedMusic;
				MusicPlayer.controls.play();
			}

			PluginConnector.Action(this, "Log", "Media: Play -> " + SelectedMusic);
			Console.WriteLine("Media: Play -> " + SelectedMusic);
		});

		PluginConnector.AddRequestHandler("StopMusic", (e) => {
			MusicPlayer.controls.pause();
		});
	}

	public void OnClose() {

	}

	public void OnMessage(string Request, Dictionary<string, object> Params) {

	}
}

static class Program {
	static void Main() {
		using (MediaPlugin M = new MediaPlugin()) {
			PluginConnector.Initialize();
			PluginConnector.Connect(M);
			PluginConnector.Run();
		}
	}
}