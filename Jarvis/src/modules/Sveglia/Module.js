
var alarms = [];
var tmp_alarm = {
    day: "",
    hours: "",
};

var Sveglia = {
    Name: "Sveglia",
    Version: "0.1",
    Help: "Svegliami domani alle 8.00; Metti la sveglia per domenica;",
    Color: "violet",
    Dependency: null,
    
    TagContextHandlers: [
        "sveglia", 
        "svegliami"
    ],
    
    TagExecHandlers: [
       {
          Tags: TAGS_DATE,
          Handler: function(T, W) {
              TAGS_DATE.forEach(function(v) {
                  if(typeof v == "string")
                     if(T.startsWith(v.slice(0, -1)))
                        tmp_alarm.day = v;

                  if(tmp_alarm.day == "")
                     tmp_alarm.day = T;
              });

              checkAlarm(T, W);
          }
       },
       
       {
          Tags: TAGS_TIME, /* 8:00, 12:45, ecc... */
          Handler: function(T, W) {
              tmp_alarm.hours = T;
              checkAlarm(T, W);
          }
       },
       
       {
           Tags: ["sveglia", "svegliami"],
           Handler: function(T, W) {
               checkAlarm(T, W);
           }
       },
    ],
    
    Initialize: function() {
        return;
    }
};


var checkAlarm = function(T, W) {
    if(tmp_alarm.day == "") {
        if(!TAGS_DATE.filter(function(v, i, a) {
            return Jarvis.TagCompare(v, W);
        }).length) {
            Jarvis.StopRecognizing();
            Jarvis.Talk("Quando?");
        }
            
        return;
    }
    
    if(tmp_alarm.hours == "") {
        if(!Jarvis.TagCompare(TAGS_TIME[0], W)) {
            Jarvis.StopRecognizing();
            Jarvis.Talk("A che ora?");
        }
            
        return;
    }
    
    Jarvis.TalkRandom(RESPONSE_OK);
    Jarvis.Talk("Ho impostato la sveglia per " + tmp_alarm.day + " alle ore: " + tmp_alarm.hours);

    alarms.push({ day: tmp_alarm.day, hours: tmp_alarm.hours });
    tmp_alarm.day = "";
    tmp_alarm.hours = "";
}

Jarvis.LoadContext(Sveglia);