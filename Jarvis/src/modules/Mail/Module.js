var Mail = {
    Name: "Mail",
    Version: "0.1",
    Help: "Inviami una e-mail ad Antonio con scritto \"Ciao come stai?\"",
    Color: "green",
    Dependency: null,
    
    TagContextHandlers: [
        "mail", "email", "e-mail", "posta"
    ],
    
    TagExecHandlers: [
        {
            Tags: [/\b(a|ad|al|alla)\s+\w+\b/],
            Handler: function(T, W) {
                MailDest = T.substr(T.indexOf(" ") + 1);
            }
        },
        {
            Tags: ["scritto", "scrivi"],
            Handler: function(T, W) {
                MailMsg = W.substr(W.indexOf(T) + T.length + 1);
            }
        },

        {
            Tags: ["invia", "inviami", "manda", "mandami"],
            Handler: function(T, W) {
                Jarvis.StopRecognizing();

                if(MailDest == "") {
                    Jarvis.Talk("A chi la devo inviare?"); 
                    return Jarvis.AnswerMe(T, function(e) {
                        MailDest = e;
                    });
                }

                if(MailMsg == "") {
                    Jarvis.Talk("Cosa scrivo?"); 
                    return Jarvis.AnswerMe(T, function(e) {
                        MailMsg = e;
                    });
                }

                if(MailObj == "") {
                    Jarvis.Talk("Qual è il titolo?"); 
                    return Jarvis.AnswerMe(T, function(e) {
                        MailObj = e;
                    });
                }


                Jarvis.HTTPRequest(
                  "http://geekstribe.altervista.org/ksys/api.php",
                  "GET",
                  [
                      [ "q", "mail" ],
                      [ "dest", btoa(MailDest) ],
                      [ "obj", btoa(MailObj) ],
                      [ "msg", btoa(MailMsg) ]
                  ],
                  function(r) {
                      if(r == "true")
                         Jarvis.Talk("Ho inviato l'email a: " + MailDest);
                      else
                         Jarvis.Talk("Non riesco ad inviare l'email a: " + MailDest);

                      Jarvis.Log("Mail: Send mail to " + MailDest + ": " + r);
                      Jarvis.Log("Object: " + MailObj + "\n" + MailMsg);

                      MailMsg = "";
                      MailDest = "";
                      MailObj = "";
                  }
              );
            }
        },
    ],
    
    Initialize: function() {
        return;
    }
};

var MailMsg = "";
var MailDest = "";
var MailObj = "";

Jarvis.LoadContext(Mail);