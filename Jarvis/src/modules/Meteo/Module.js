var openweather_appid="b9ee557a2e6137c0f786d6855b537e4e";
var openweather_city="Gioia Tauro";

var Meteo = {
    Name: "Meteo",
    Version: "0.1",
    Help: "Che tempo fa a Roma? Com'è il meteo di oggi?",
    Color: "green",
    Dependency: null,
    
    TagContextHandlers: [
        "meteo", "tempo"
    ],
    
    TagExecHandlers: [
       {
           Tags: [/\b(a|di)\s+\w+\b/],
           Handler: function(T, W) {
               openweather_city = T.substr(2);
           }
       },

       {
          Tags: ["mostrami", "dimmi", "com'è", "come", "che", /\b(a|di)\s+\w+\b/],
          Handler: function(T, W) {
              Jarvis.StopRecognizing();
              Jarvis.HTTPRequest(
                  "http://api.openweathermap.org/data/2.5/weather",
                  "GET",
                  [
                      ["q", openweather_city],
                      ["appid", openweather_appid]
                  ],
                  function(json) {
                      var r = JSON.parse(json);
                      
                      Jarvis.Talk(
                          "La temperatura a {location} è di {temp}° con un cielo " + r.weather[0].description,
                          [
                              "{location} : " + r.name,
                              "{temp} : " + Math.floor(r.main.temp - 273.15),
                              "clear sky : sereno",
                              "few clouds : poco nuvoloso",
                              "light clouds : leggermente nuvoloso",
                              "scattered clouds : leggermente nuvoloso",
                              "broken clouds : nuvoloso",
                              "light rain : leggermente piovoso",
                              "shower rain : leggermente piovoso",
                              "moderate rain : moderatamente piovoso",
                              "rain : piovoso",
                              "thunderstorm : tempestoso",
                              "snow : nevoso",
                              "mist : nuvoloso con presenza di nebbia"
                          ]
                      );

                       Jarvis.HTTPRequest(
                            "http://api.openweathermap.org/data/2.5/weather",
                            "GET",
                            [
                                ["q", openweather_city],
                                ["appid", openweather_appid],
                                ["mode", "html"]
                            ],
                            function(html) {
                                if(!Jarvis.GetWindow())
                                    return;

                                Jarvis.GetWindow().document.getElementById("content").innerHTML = html;
                            }
                        );
                  }
              );
          }
       },
    ],
    
    Initialize: function() {
        return;
    }
};

Jarvis.LoadContext(Meteo);