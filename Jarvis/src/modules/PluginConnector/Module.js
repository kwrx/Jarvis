var PluginConnector = {
    Name: "PluginConnector",
    Version: "0.1",
    Color: "yellow",
    Dependency: null,
    
    TagContextHandlers: [],
    TagExecHandlers: [],
    
    Initialize: function() {
        chrome.sockets.tcpServer.create({}, function(e) {
            chrome.sockets.tcpServer.listen(
                e.socketId,
                "127.0.0.1", 5500,
                function(r) {
                    if(r < 0)
                        return Jarvis.Log("PluginConnector: " + chrome.runtime.lastError.message);

                    chrome.sockets.tcpServer.onAccept.addListener(function(i) {
                        if(i.socketId != e.socketId)
                            return;

                        chrome.sockets.tcp.onReceive.addListener(function(recv) {
                            if(recv.socketId != i.clientSocketId)
                                return;

                            onReceiveData(recv.socketId, String.fromCharCode.apply(null, new Uint16Array(recv.data)));
                        });

                        chrome.sockets.tcp.setPaused(i.clientSocketId, false);
                    });
                });
        });
    },

    SendRequest: function(plugin, request, params) {
        /** Params:
         * [ "name" : "value" ], [ "name" : "value" ], ...
         */

        var data = JSON.stringify({
            Request: request,
            Params: params
        }) + "$";

        var ab = new ArrayBuffer(data.length * 2);
        for(var i = 0, b = new Uint16Array(ab); i < data.length; i++)
            b[i] = data.charCodeAt(i);

        PluginsConnected.forEach(function(v) {
            if(v.name != plugin)
                return;

            chrome.sockets.tcp.send(v.socketId, ab, function(e) {
                if(e.resultCode < 0)
                    return Jarvis.Log("PluginConnector: SendRequest (" + plugin + "->" + request + ") failed with error: " + e.resultCode);

                Jarvis.Log("PluginConnector: SendRequest (" + v.socketId + ", " + plugin + "->" + request + ") (" + e.bytesSent + " Bytes) sent");
            });
        });
    }
};



var PluginsConnected = [];
var onReceiveData = function(sid, data) {
    var res = JSON.parse(data);
    if(!res)
        return Jarvis.Log("PluginConnector: invalid received data");

    switch(res.type) {
        case 0: /* PLUGIN_CONNECT */
            res.data.socketId = sid;
            PluginsConnected.push(res.data);

            Jarvis.Log("PluginConnector: " + res.name + " connected with id " + sid);
            break;
        case 1: /* PLUGIN_DISCONNECT */
            PluginsConnected = PluginsConnected.filter(function(v, i, a) {
                if(v.name == res.name)
                    return false;

                return true;
            });

            Jarvis.Log("PluginConnector: " + res.name + " disconnected");
            break;
        case 2: /* PLUGIN_ACTION */
            Jarvis.Log("PluginConnector: Received Request from " + res.name);
            Jarvis.Log(res.data);

            switch(res.data.Do) {
                case "Log":             Jarvis.Log(res.data.Params[0]); break;
                case "Talk":            Jarvis.Talk(res.data.Params[0], res.data.Params[1]); break;
                case "TalkRandom":      Jarvis.TalkRandom(res.data.Params); break;
                case "StopTalking":     Jarvis.StopTalking(); break;
                case "Recognize":       Jarvis.Recognize(res.data.Params[0], function(e) {
                                            PluginConnector.SendRequest(res.name, "Recognize_OnFail", [[ "0", e ]]);
                                        }); break;
                case "StopRecognizing": Jarvis.StopRecognizing(); break;
                case "Listen":          Jarvis.Listen(function(e) {
                                            PluginConnector.SendRequest(res.name, "Listen_OnResult", [[ "0", e ]]);
                                        }); break;
                case "TagCompare":      PluginConnector.SendRequest(res.name, "TagCompare_OnResult", 
                                            [[ "0", Jarvis.TagCompare(res.data.Params[0], res.data.Params[1]) ]]
                                        ); break;
                case "TagFound":        PluginConnector.SendRequest(res.name, "TagFound_OnResult",
                                            [[ "0", Jarvis.TagFound(res.data.Params[0], res.data.Params[1]) ]]
                                        ); break;
                case "Close":           Jarvis.Close();
                default:
                    Jarvis.Log("PluginConnector: invalid action \"" + res.data.Do + "\" from " + res.name);
                    break;
            }
            break;
        default:
            Jarvis.Log("PluginConnector: invalid request type " + res.type + " from " + res.name);
            break;
    }
};

Jarvis.LoadContext(PluginConnector);