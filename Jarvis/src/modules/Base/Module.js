var Base = {
    Name: "Base",
    Version: "0.1",
    Help: "Ciao, come stai? Che fai? Tutto bene?",
    Color: "yellow",
    Dependency: null,
    
    TagContextHandlers: [
        "ciao", "salve",
        "presentati", "chi sei", "cosa sei", "parlami di te",
        "zitto", "zitta", "silenzio", "basta",
        "a posto", "tutto a posto", "tutto ok", "tutto bene", "come stai",
        "stai bene", "come ti senti", "ci sono problemi",
        "che fai", "cosa stai facendo", "cosa fai",
    ],
    
    TagExecHandlers: [
       {
          Tags: ["ciao", "salve"],
          Handler: function(T, W) {
              Jarvis.TalkRandom(RESPONSE_HELLO)
          }
       },
       
       {
           Tags: ["a posto", "tutto ok", "tutto bene", "come stai",
                    "stai bene", "come ti senti", "ci sono problemi", "come va", "che si dice"],
           Handler: function (T, W) {
               Jarvis.TalkRandom([
                   "Tutto OK!",
                   "Tutto bene!",
                   "A posto!"
               ]);

               Jarvis.TalkRandom([
                   "",
                   "Tutto funziona correttamente",
                   "Non ci sono problemi",
                   "Mai stato meglio!"
               ]);
           }
       },

       {
           Tags: ["che fai", "cosa stai facendo", "cosa fai"],
           Handler: function (T, W) {
               Jarvis.TalkRandom([
                   "Niente",
                   "Nulla",
               ]);

               Jarvis.TalkRandom([
                   "Sono in attesa di comandi",
                   "C'è qualcosa che posso fare per te?",
                   "Cosa posso fare per te?"
               ]);
           }
       },

       {
          Tags: ["zitto", "zitta", "silenzio", "basta", "fermati", "ferma", "fermo"],
          Handler: function(T, W) {
              Jarvis.StopTalking();
          }
       },
       
       {
           Tags: ["presentati", "chi sei", "cosa sei", "parlami di te"],
           Handler: function(T, W) {
               Jarvis.Talk(
                   "Io sono Jarvis: un'intelligenza artificiale, ideata da "     +
                    "Antonio Natale per il progetto: \"esami di stato 2016\"."  +
                    "Il mio compito è quello di fornire assistenza in casa, "    +      
                    "interagendo con gli utenti mediante sistemi vocali. "      +
                    "Il mio accento fa schifo, lo so, ma non dipende da me, "   +
                    "dipende dal sistema operativo che mi ospita. ",
                    
                    ["Jarvis : Giarvis"]
               );
           }
       }
    ],
    
    Initialize: function() {
        return;
    }
};

Jarvis.LoadContext(Base);