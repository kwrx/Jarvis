var Calendar = {
    Name: "Calendar",
    Version: "0.1",
    Help: "Ricordami di studiare domani; Ricordami l'appuntamento ",
    Color: "green",
    Dependency: null,
    
    TagContextHandlers: [
        "ricordami", "ricorda"
    ],
    
    TagExecHandlers: [
        {
            Tags: TAGS_DATE,
            Handler: function(T, W) {
                CalendarDate = T;
            }
        },
        {
            Tags: [/\b(di|del|della|il|la|l'|i|gli|le)\s+\w+\b/],
            Handler: function(T, W) {
                CalendarRemind = T.substr(T.indexOf(" ") + 1);
            }
        },
        {
            Tags: ["ricordami", "ricorda"],
            Handler: function(T, W) {
                if(CalendarRemind == "") {
                    Jarvis.Talk("Cosa?");
                    return Jarvis.AnswerMe(T, function(e) {
                        CalendarRemind = e;
                    });
                }

                if(CalendarDate == "") {
                    Jarvis.Talk("Quando?");
                    return Jarvis.AnswerMe(T, function(e) {
                        TAGS_DATE.forEach(function(v) {
                            if(!Jarvis.TagCompare(v, e))
                                return;

                            CalendarDate = Jarvis.TagFound(v, e).pop(); 
                        });

                        if(CalendarDate == "")
                            Jarvis.TalkRandom(RESPONSE_NOT_UNDERSTOOD);
                    });
                }



                Jarvis.TalkRandom(RESPONSE_OK);
                Jarvis.Talk("Ho impostato un evento sul calendario per: " + CalendarDate);
                Jarvis.Log("Calendar: Set event\nDate: " + CalendarDate + "\n" + CalendarRemind);

                CalendarEvent.push({
                    Remind: CalendarRemind,
                    Date: CalendarDate,
                });

                CalendarRemind = "";
                CalendarDate = "";
            }
        },
    ],
    
    Initialize: function() {
        return;
    }
};

var CalendarEvent = [];
var CalendarRemind = "";
var CalendarDate = "";

Jarvis.LoadContext(Calendar);