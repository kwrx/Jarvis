var Room = "";

var Finestre = {
    Name: "Finestre",
    Version: "0.1",
    Help: "Apri la finestra; Puoi chiudermi la finestra?",
    Color: "blue",
    Dependency: null,
    
    TagContextHandlers: [
        "finestre", 
        "finestra"
    ],
    
    TagExecHandlers: [
        {
            Tags: ["soggiorno", "garage", "cucina", "ingresso", "casa"],
            Handler: function(T, W) {
                Room = T;
            }
        },
        {
            Tags: ["apri", "aprile", "aprila", "apriamo", "aprire", "aprimi"],
            Handler: function(T, W) {
                switch(Room) {
                    case "":
                        /*Jarvis.Talk("In tutta la casa?");
                        return Jarvis.AnswerMe(T, function(e) {
                            if(e.search("sì") != -1)
                                Room = "casa";
                            else
                                Room = /(soggiorno|cucina|garage|ingresso)/.exec(Room);

                            if(Room != "" && Room != undefined)
                                return;

                            Jarvis.TalkRandom(RESPONSE_NOT_UNDERSTOOD);
                            Room = "";
                        });*/
                    case "soggiorno":
                        PluginConnector.SendRequest("Home", "LivingDoorsOpen");
                        break;
                    case "cucina":
                        PluginConnector.SendRequest("Home", "KitchenDoorsOpen");
                        break;
                    case "garage":
                        PluginConnector.SendRequest("Home", "GarageDoorsOpen");
                        break;
                    case "ingresso":
                        PluginConnector.SendRequest("Home", "EntranceDoorsOpen");
                        break;
                    case "casa":
                        PluginConnector.SendRequest("Home", "AllDoorsOpen");
                        break;
                }

                Jarvis.TalkRandom(RESPONSE_OK);
            }
        },

        {
            Tags: ["chiudi", "chiudile", "chiudila", "chiudiamo", "chiudere", "chiudimi", "chiude"],
            Handler: function(T, W) {
                switch(Room) {
                    case "":
                        /*Jarvis.Talk("In tutta la casa?");
                        return Jarvis.AnswerMe(T, function(e) {
                            if(e.search("sì") != -1)
                                Room = "casa";
                            else
                                Room = /(soggiorno|cucina|garage|ingresso)/.exec(Room);

                            if(Room == "" || !Room)
                                Jarvis.TalkRandom(RESPONSE_NOT_UNDERSTOOD);
                        });*/
                    case "soggiorno":
                        PluginConnector.SendRequest("Home", "LivingDoorsClose");
                        break;
                    case "cucina":
                        PluginConnector.SendRequest("Home", "KitchenDoorsClose");
                        break;
                    case "garage":
                        PluginConnector.SendRequest("Home", "GarageDoorsClose");
                        break;
                    case "ingresso":
                        PluginConnector.SendRequest("Home", "EntranceDoorsClose");
                        break;
                    case "casa":
                        PluginConnector.SendRequest("Home", "AllDoorsClose");
                        break;
                }

                Jarvis.TalkRandom(RESPONSE_OK);
            }
        }
        ],

        Initialize: function() {
            return;
        }
};

Jarvis.LoadContext(Finestre);