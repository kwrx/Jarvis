var Room = "";

var Luci = {
    Name: "Luci",
    Version: "0.1",
    Help: "Apri le luci; Puoi chiudermi la luce?",
    Color: "yellow",
    Dependency: null,
    
    TagContextHandlers: [
        "luci", 
        "luce",
        "illuminazione",
        "illuminazioni"
    ],
    
    TagExecHandlers: [
        {
            Tags: ["soggiorno", "garage", "cucina", "ingresso", "casa"],
            Handler: function(T, W) {
                Room = T;
            }
        },
        {
            Tags: ["apri", "aprile", "aprila", "apriamo", "aprire", "aprimi", "apre",
                    "accendi", "accendile", "accendila", "accendiamo", "accendere", "accendimi", "accende"],
            Handler: function(T, W) {
                switch(Room) {
                    case "":
                        /*Jarvis.Talk("In tutta la casa?");
                        return Jarvis.AnswerMe(T, function(e) {
                            if(e.search("sì") != -1)
                                Room = "casa";
                            else
                                Room = /(soggiorno|cucina|garage|ingresso)/.exec(Room);

                            if(Room != "" && Room != undefined)
                                return;

                            Jarvis.TalkRandom(RESPONSE_NOT_UNDERSTOOD);
                            Room = "";
                        });*/
                    case "soggiorno":
                        PluginConnector.SendRequest("Home", "LivingLightsOpen");
                        break;
                    case "cucina":
                        PluginConnector.SendRequest("Home", "KitchenLightsOpen");
                        break;
                    case "garage":
                        PluginConnector.SendRequest("Home", "GarageLightsOpen");
                        break;
                    case "ingresso":
                        PluginConnector.SendRequest("Home", "EntranceLightsOpen");
                        break;
                    case "casa":
                        PluginConnector.SendRequest("Home", "AllLightsOpen");
                        break;
                }

                Jarvis.TalkRandom(RESPONSE_OK);
            }
        },

        {
            Tags: ["chiudi", "chiudile", "chiudila", "chiudiamo", "chiudere", "chiudimi", "chiude",
                    "spegni", "spegnile", "spegnila", "spegniamo", "spegnere", "spegnimi", "spegne"],
            Handler: function(T, W) {
                switch(Room) {
                    case "":
                        /*Jarvis.Talk("In tutta la casa?");
                        return Jarvis.AnswerMe(T, function(e) {
                            if(e.search("sì") != -1)
                                Room = "casa";
                            else
                                Room = /(soggiorno|cucina|garage|ingresso)/.exec(Room);

                            if(Room == "" || !Room)
                                Jarvis.TalkRandom(RESPONSE_NOT_UNDERSTOOD);
                        });*/
                    case "soggiorno":
                        PluginConnector.SendRequest("Home", "LivingLightsClose");
                        break;
                    case "cucina":
                        PluginConnector.SendRequest("Home", "KitchenLightsClose");
                        break;
                    case "garage":
                        PluginConnector.SendRequest("Home", "GarageLightsClose");
                        break;
                    case "ingresso":
                        PluginConnector.SendRequest("Home", "EntranceLightsClose");
                        break;
                    case "casa":
                        PluginConnector.SendRequest("Home", "AllLightsClose");
                        break;
                }

                Jarvis.TalkRandom(RESPONSE_OK);
            }
        }
        ],

        Initialize: function() {
            return;
        }
};

Jarvis.LoadContext(Luci);