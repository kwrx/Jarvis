var Room = "";
var Temp = 20;

var Riscaldamenti = {
    Name: "Riscaldamenti",
    Version: "0.1",
    Help: "Apri i Riscaldamenti; Mettili a 20°?",
    Color: "red",
    Dependency: null,
    
    TagContextHandlers: [
        "riscaldamenti", 
        "riscaldamento",
        "climatizzatore",
        "climatizzatori"
    ],
    
    TagExecHandlers: [
        {
            Tags: [/\b\d+\s+gradi\b/],
            Handler: function(T, W) {
                Temp = T.substr(0, T.indexOf(" "));
            }
        },
        {
            Tags: ["soggiorno", "garage", "cucina", "ingresso", "casa"],
            Handler: function(T, W) {
                Room = T;
            }
        },
        {
            Tags: ["apri", "aprili", "aprile", "aprila", "apriamo", "aprire", "aprimi", "apre",
                    "accendi", "accendili", "accendile", "accendila", "accendiamo", "accendere", "accendimi", "accende",
                    "metti", "mettili", "mettile", "mettila", "mettiamo", "mettere", "mettimi", "mette",
                    "imposta", "impostali", "impostale", "impostala", "impostiamo", "impostare"],
            Handler: function(T, W) {
                switch(Room) {
                    case "":
                        /*Jarvis.Talk("In tutta la casa?");
                        return Jarvis.AnswerMe(T, function(e) {
                            if(e.search("sì") != -1)
                                Room = "casa";
                            else
                                Room = /(soggiorno|cucina|garage|ingresso)/.exec(Room);

                            if(Room != "" && Room != undefined)
                                return;

                            Jarvis.TalkRandom(RESPONSE_NOT_UNDERSTOOD);
                            Room = "";
                        });*/
                    case "soggiorno":
                        PluginConnector.SendRequest("Home", "LivingHeatOpen", [["0", Temp]]);
                        break;
                    case "cucina":
                        PluginConnector.SendRequest("Home", "KitchenHeatOpen", [["0", Temp]]);
                        break;
                    case "garage":
                        PluginConnector.SendRequest("Home", "GarageHeatOpen", [["0", Temp]]);
                        break;
                    case "ingresso":
                        PluginConnector.SendRequest("Home", "EntranceHeatOpen", [["0", Temp]]);
                        break;
                    case "casa":
                        PluginConnector.SendRequest("Home", "AllHeatOpen", [["0", Temp]]);
                        break;
                }

                Jarvis.TalkRandom(RESPONSE_OK);
                Jarvis.Talk("Ho acceso i riscaldamenti a {temp}°", ["{temp} : " + Temp]);
            }
        },

        {
            Tags: ["chiudi", "chiudile", "chiudila", "chiudiamo", "chiudere", "chiudimi", "chiude",
                    "spegni", "spegnile", "spegnila", "spegniamo", "spegnere", "spegnimi", "spegne"],
            Handler: function(T, W) {
                switch(Room) {
                    case "":
                        /*Jarvis.Talk("In tutta la casa?");
                        return Jarvis.AnswerMe(T, function(e) {
                            if(e.search("sì") != -1)
                                Room = "casa";
                            else
                                Room = /(soggiorno|cucina|garage|ingresso)/.exec(Room);

                            if(Room == "" || !Room)
                                Jarvis.TalkRandom(RESPONSE_NOT_UNDERSTOOD);
                        });*/
                    case "soggiorno":
                        PluginConnector.SendRequest("Home", "LivingHeatClose");
                        break;
                    case "cucina":
                        PluginConnector.SendRequest("Home", "KitchenHeatClose");
                        break;
                    case "garage":
                        PluginConnector.SendRequest("Home", "GarageHeatClose");
                        break;
                    case "ingresso":
                        PluginConnector.SendRequest("Home", "EntranceHeatClose");
                        break;
                    case "casa":
                        PluginConnector.SendRequest("Home", "AllHeatClose");
                        break;
                }

                Jarvis.TalkRandom(RESPONSE_OK);
            }
        }
        ],

        Initialize: function() {
            return;
        }
};

Jarvis.LoadContext(Riscaldamenti);