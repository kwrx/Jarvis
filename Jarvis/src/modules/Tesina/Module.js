var TesinaVersion = "1.0.0.0";
var TesinaHelp = "Parlami di questo autore, ecc...";
var TesinaColor = "red";
var TesinaDependency = null;

var TesinaDB = [
    [
        ["tesina", "tesi", "elaborato"],
        "La tesina tratta come argomento principale: l'intelligenza artificiale. La scelta ricade su di essa,   \
        in quanto ha origine dalla passione verso la tecnologia, l'informatica, la logica, la ragione e         \
        l'intelligenza umana. Intelligenza che permette di fare scelte, di discernere ed imparare i criteri     \
        per compiere quest'ultime. Intelligenza che risiede in quell'organo, il cervello, tanto complesso       \
        quanto sconosciuto.",
        [],
    ],
    [
        [ "primo levi", "levi" ],
        "Primo Levi nasce a Torino nel 1919 da una ricca famiglia ebrea di tradizioni intellettuali.        \
        Nel 1941 si laurea in chimica nonostante l'ostacolo delle leggi razziali. Dopo l'8 settembre 1943,  \
        la disfatta dell'esercito italiano e l'occupazione nazista dell'Italia, Levi aderisce a una         \
        formazione partigiana di \"Giustizia e Libertà\", ma viene arrestato dalla milizia repubblichina.   \
        Consegnato ai tedeschi viene deporato ad Auschwitz nel gennaio 1944. Sopravvissuto al lager, viene  \
        liberato nel gennaio del 1945 dall'Armata Rossa e, per quasi un anno, è al seguito delle truppe     \
        sovietiche in un'odissea che lo conduce lungo un itinerario impazzito per tutta l'Europa orientale. \
        Soltanto nell'ottobre del 1945 riesce a tornare a casa. Esordisce nel 1947 con Se questo è un uomo, \
        testimonianza della prigionia patita nei campi di concentramento nazisti e della lotta per la       \
        sopravvivenza, non solo fisica ma anche della propria dignità di uomo.                              \
        Il romanzo successivo, La tregua (1963 premio Campiello) dà una descrizione del ritorno alla vita   \
        dopo quell'atroce esperienza. Pubblica in seguito altri romanzi, saggi, raccolte di poesie (Osteria \
        di Brema, 1975; Ad ora incerta, 1984) e numerosi racconti. Muore suicida nel 1987.                  \
        Il nome di Primo Levi è principalmente legato alla testimonianza degli orrori della guerra e della  \
        Shoah contenuta nelle celebri pagine di Se questo è un uomo. La riflessione sull'atroce esperienza  \
        del lager ritorna però anche in altre opere di questo autore, da Se non ora, quando? (1982) a I     \
        sommersi e i salvati (1986), intrecciandosi con una lucida analisi e critica della società          \
        contemporanea. Fra le tante opere di Levi ricordiamo: Il sistema periodico (1975),                  \
        dove i vari elementi chimici vengono utilizzati come spunto per raccontare la formazione morale     \
        e civile di un giovane ebreo, e La chiave a stella (1978), celebrazione della professionalità di    \
        un operaio raccontata come esempio di una scelta di grande rigore morale.",
        [
            [
                [ "se questo è un uomo" ],
                "Se questo è un uomo è un'opera memorialistica di Primo Levi scritta tra il dicembre 1945 ed    \
                il gennaio 1947. Rappresenta la coinvolgente ma meditata testimonianza di quanto vissuto        \
                dall'autore nel campo di concentramento di Auschwitz. Levi sopravvisse infatti alla deportazione\
                nel campo di Monowitz, lager satellite del complesso di Auschwitz.",
            ],
            [
                [ "comunicazione" ],
                "Ad animare la più importante produzione narrativa di Levi è la volontà di comunicare con i     \
                suoi simili, di condividerne l'esperienza per capire, di fornire una testimonianza oggettiva    \
                e documentaria della tragica esperienza del lager. In questo approccio scientifico di fiducia   \
                nella ragione umana, quale unico mezzo di cui dispone l'uomo per emergere dall'orrore e         \
                dall'assurdo, risiede il suo profondo rispetto per la dignità umana.                            \
                La scelta ricade quindi, su una lingua chiara e limpida, espressa in uno stile asciutto con un  \
                taglio prettamente scientifico.",
            ],
        ],
    ],

    [
        ["giovanni pascoli", "pascoli" ],
        "Nacque nel 1855 a San Mauro di Romagna ed era quarto di 10 figli. Fin da piccolo ebbe buoni        \
        insegnanti che gli trasmisero la passione per i classici. Purtroppo nel 1867 il padre fu            \
        assassinato tornando da un viaggio a Cesena, e questo gli segnò la fine dell’infanzia e l’ingresso  \
        al mondo degli adulti.                                                                              \
        Nel giro di pochi anni morirono altri parenti e per Pascoli si era rotto ciò che lui definiva       \
        “nido” familiare. Intanto le condizioni economiche stavano peggiorando però, grazie ad una borsa    \
        di studio riuscì a continuare gli studi a Bologna nella facoltà di lettere. Però durante questi     \
        anni visse in un periodo di crisi, preoccupato per le difficoltà economiche e per la lontananza     \
        dalla famiglia. Pascoli fu arrestato per aver partecipato a una manifestazione a favore degli       \
        anarchici, ma fu presto liberato grazie all’aiuto di Giosuè Carducci. Pascoli finì gli studi,       \
        si laureò e iniziò ad insegnare latino e greco. A 38 anni pubblicò il Myricae, una raccolta poetica.\
        In seguito comprò una casetta a Castelvecchio dove visse con suo sorella Maria (Mariù) e cerco di   \
        ricostruire il nido famigliare.                                                                     \
        A 50 anni fu nominato insegnante di lettere all’università di Bologna come successore di Carducci.  \
        Nell’ultimo periodo della sua vita scrisse altre opere come i Canti di Castelvecchio e morì di      \
        malattia a 57 anni.",
        [
            [
                [ "fanciullino" ],
                "I tratti più significativi   \
                della sua poetica sono descritti ne “Il fanciullino” scritto nel 1897, in cui viene affermata   \
                la natura irrazionale e intuitiva della creazione artistica. Pascoli considera la poesia come   \
                ricordo del momento magico, legato all’età infantile, in cui il bambino scopre nelle cose che   \
                lo circondano, anche nelle più umili e consuete, il senso nascosto e segreto. Mentre gli uomini \
                comuni, crescendo e diventando adulti, perdono la capacità di guardare con stupore ciò che      \
                vedono.",
            ],

            [
                [ "nido" ],
                "Per pascoli il “nido” è il simbolo più frequente nelle sue poesia, e lo compara al nido di     \
                “casa”, luogo di protezione, o “culla” segno della regressione all’infanzia, fino al nido       \
                “vuoto”, il cimitero, dove i morti tornano a confortare chi è rimasto in vita.",
            ],

            [
                [ "simbolismo" ],
                "Nella poesia francese nella seconda metà dell’Ottocento i poeti vogliono presentare la realtà  \
                attraverso una visione soggettiva e personale, in sintonia con il loro stato d’animo, filtrando \
                il mondo esterno attraverso suoni, immagini e colori, ritrovando negli elementi naturali una    \
                corrispondenza con le emozioni. La rottura rispetto ai temi e alle scelte stilistiche del       \
                Romanticismo viene compiuta da una raccolta di poesie che nasce dalla sofferenza e dalle        \
                tenebre: I Fiori del male di Charles Baudelaire, i cui temi tipici erano lo squallore della     \
                vita contemporanea, del brutto e del diverso. Il poeta s’immagina come un albatro che la il     \
                cielo per camminare goffo sulla terra, trovandosi in un “mondo” di cui non comprende il senso.",
            ],
        ]
    ],

    [
        [ "giovanni verga", "verga" ],
        "Giovanni Verga,nata a Catania nel 1840 in un'agiata famiglia di proprietari terrieri,mostra presto   \
        un particolare interesse per la letteratura.Educato in un ambiente di idee liberali,a soli quindi     \
        anni scrive il suo primo romanzo,\"Amore e patria\"; ben presto si dedica interamente all'attività    \
        di scrittore e giornalista abbandonando gli studi universitari di Legge. All'arrivo dei Mille(1860)   \
        Giovanni Verga si arruola nella Guardia Nazionale Garibaldina,partecipando anche ad azioni            \
        militari.                                                                                             \
        Dal 1869 soggiorna a Firenze,frequentando gli ambienti letterari, e conosce gli scrittori Luigi       \
        Capuana e Federico De Roberto che,con Verga,saranno i principali esponenti di un nuovo movimento      \
        letterario:il Verismo.Nel 1871 Giovanni Verga pubblica \"Storia di una capinera\", il suo primo       \
        romanzo di successo.L'anno successivo si trasferisce a Milano,dove partecipa all'intensa vita         \
        intellettuale della città.Legge e apprezza opere di autori di quel periodo,come Balzac,Flaubert e     \
        Zola,rappresentanti delle correnti letterarie del Realismo e del Naturalismo francese,che assegna     \
        alla letteratura il compito di studiare con metodo scientifico la società e la psicologia dell'uomo.  \
        Con la raccolta di novelle \"Vita dei campi\"(1880),e poi i \"Malavoglia\"(1881),le                   \
        \"Novelle rusticane\"(1883),il \"Mastro-don Gesualdo\"(1889), Giovanni Verga rivolge l'attenzione     \
        al mondo degli uomini,dei senza speranza,degli uomini destinati a soccombere nella dura lotta per     \
        l'esistenza.La novità della sua produzione \"verista\" lascia perplesso il pubblico che non comprende \
        la sua grandezza.Nel 1893 lo scrittore Giovanni Verga si ritira a Catania dove trascorre gli ultimi   \
        trent'anni della sua vita,in solitudine,lontano da ogni impegno,scrivendo pochissimo. Muore nel 1922.",

        [
            [
                [ "malavoglia" ],
                "Fu pubblicata nel 1881, l'opera più famosa ed importante di Verga, e fa parte del              \
                \"circolo dei vinti\", insieme alla sua altra opera \"Mastro Don Gesualdo\". I protagonisti     \
                dei Malavoglia sono un'intera famiglia catanese che vive di pesca: I Toscano. L'opera prende    \
                il nome dal soprannome della famiglia, la quale è chiamata così in quanto era una famiglia che  \
                si dava molto da fare per ciò che riguarda il lavoro, anche se il soprannome con il suo         \
                significato moderno potrebbe trarre in inganno e far pensare il contrario. La famiglia è        \
                abbastanza numerosa, ed economicamente non sta male, in quanto hanno una casa e una piccola     \
                barca. Il primo genito, il ventenne 'Ntony sarà chiamato per svolgere il servizio militare,     \
                cioè la leva, e ciò metterà in difficoltà il padre, il quale si ritroverà con un uomo in meno e \
                avrà bisogno di nuova forza lavoro... per ottenere tale forza lavora quest'uomo chiederà un     \
                prestito ad un usuraio, prestito con il quale farà degli acquisti che gli andranno a fruttare   \
                molto. Grazie ai soldi guadagnati il capostipite della famiglia potrà finanziare il matrimonio  \
                della figlia. Il matrimonio in realtà è un matrimonio d'interesse, come accadeva spesso nella   \
                Sicilia del XIX secolo, difatti la ragazza non e innamorata del marito ma bensì di un altro     \
                uomo. Si susseguiranno varie disgrazie, le quali saranno raccontate in modo impersonale         \
                attraverso i personaggi del villaggio che faranno il loro ingresso nell'opera solo in seguito...",
            ],
        ]
    ],
    [
        [ "processo", "processi" ],
        "Il processo è un attività svolta con continuità all'interno delle organizzazioni, costituite da una    \
        sequenza di operazioni note, che vengono eseguite per realizzare gli obiettivi dell'organizzazione.",
        [],
    ],

    [
        [ "progetto", "progetti" ],
        "Un progetto è un insieme di attività che realizza cambiamenti all'interno delle organizzazioni in      \
        in risposta alle mutate condizioni del contesto, ad esempio: il mercato, la società civile o gli        \
        obiettivi dell'organizzazione",
        [],
    ],

    [
        [ "sicurezza" ],
        "Con il termine sicurezza dei dati si intende, nell'ambito della sicurezza informatica e delle          \
        telecomunicazioni, la protezione dei dati e delle informazioni nei confronti delle modifiche del        \
        contenuto, accidentali oppure effettuate da una terza parte.",
        [],
    ],
    [
        [ "crittografia" ],
        "La crittografia è una tecnica di rappresentazione di un messaggio in una forma tale che l’informazione in esso     \
        contenuta possa essere recepita solo dal destinatario. Ciò si può ottenere con due diversi metodi. Celando          \
        l’esistenza stessa del messaggio oppure sottoponendo il testo del messaggio a trasformazioni che lo rendano         \
        incomprensibile.",
        [
            [
                [ "simmetrica" ],
                "Con crittografia simmetrica, o crittografia a chiave privata, si intende una tecnica di        \
                cifratura. Rappresenta un metodo semplice per cifrare testo in chiaro dove la chiave di         \
                crittazione è la stessa chiave di decrittazione, rendendo l'algoritmo molto performante e       \
                semplice da implementare. Alcuni esempi sono: AES, DES e triplo DES.",
            ],
            [
                [ "asimmetrica" ],
                "La crittografia asimmetrica, è un tipo di crittografia dove, ad ogni attore coinvolto nella    \
                comunicazione è associata una coppia di chiavi.                                                 \
                Una chiave pubblica, che deve essere distribuita e una chiave privata, appunto personale e      \
                segreta. Alcuni esempi sono: RSA, PGP, SSH e DSS.",
            ]
        ]
    ],
    [
        [ "dbms" ],
        "In informatica, un DBMS o Sistema di gestione di basi di dati è un sistema software progettato per     \
        consentire la creazione, la manipolazione e l'interrogazione efficiente di un database.",
        [],
    ],
    [
        ["database"],
        "Un database o base di dati indica un insieme organizzato di dati. Le informazioni sono organizzate e   \
        e collegate tra loro secondo un particolare modello logico scelto. Ad esempio: relazionale, gerarchico o\
        a oggetti. Gli utenti si interfacciano ad esso attraverso i cosiddetti linguaggi di query e grazie a    \
        particolari applicazioni come i DBMS.",
        [],
    ],
    [
        ["chiave esterna", "chiavi esterne"],
        "Nel contesto dei database relazionali, una chiave esterna è un vincolo di integrità referenziale tra   \
        due o più tabelle. Essa identifica una o più colonne di una tabella che referenzia una o più colonne di \
        un'altra tabella.",
        [],
    ],
    [
        ["chiave primaria", "chiavi primarie"],
        "Nel modello relazionale della basi di dati la chiave primaria è un insieme di attributi che permette   \
        di individuare univocamente un record o tupla o ennupla in una tabella o relazione.",
        [],
    ],
    [
        [ "sql" ],
        "SQL è il linguaggio di definizione e manipolazione dei dati universalmente usato nelle basi di dati relazionali.",
        [],
    ],
    [
        [ "intelligenza artificiale" ],
        "L'intelligenza artificiale è l'abilità di un computer di svolgere funzioni e ragionamenti tipici della mente umana.",
        [
            [
                [ "forte" ],
                "L'intelligenza artificiale forte è un sistema in grado di pensare e avere una mente propria.        \
                Questa teoria si sviluppa da un concetto che risale dal filosofo Thomas Hobbes. Il quale sosteneva   \
                che \"ragionare non è nient'altro che calcolare\"."
            ],
            [
                ["debole"],
                "L'intelligenza artificiale debole è un sistema che agisce come se pensasse e avesse una mente propria."
            ]
        ]
    ]
];

TesinaDB.forEach(function(v) {
    var C = {};

    C.Name = "Tesina - " + 
        (v[0][0]
            .replace(/\s+\w/g, function(x) { return x.toUpperCase(); })
            .replace(/\w/, function(x) { return x.toUpperCase(); })
        );

    C.Version = TesinaVersion;
    C.Help = TesinaHelp;
    C.Color = TesinaColor;
    C.Dependency = TesinaDependency;

    C.TagContextHandlers = v[0];
    C.TagExecHandlers = [];
    
    v[2].forEach(function(e) {
        C.TagExecHandlers.push({
            Tags: e[0],
            Handler: function(T, W) {
                Jarvis.StopRecognizing();
                Jarvis.Talk(e[1]);
            }
        });
    });

    C.TagExecHandlers.push({
        Tags: v[0],
        Handler: function(T, W) {
            Jarvis.StopRecognizing();
            Jarvis.Talk(v[1]);
        }
    });

    C.Initialize = function() {};
    Jarvis.LoadContext(C);
});