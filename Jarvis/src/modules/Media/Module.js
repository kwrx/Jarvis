var Media = {
    Name: "Media",
    Version: "0.1",
    Help: "Metti un po' di articolo; Fammi sentire Certe Notti di Ligabue",
    Color: "green",
    Dependency: null,
    
    TagContextHandlers: [
        "musica", "canzone", "canzoni", "brano", "brani", "sentire", "ascoltare", "album", "disco", "dischi"
    ],
    
    TagExecHandlers: [

       {
           Tags: [/\b(\w*\s*)\b(degli|di|dei|i)\s+(\w+)\b/],
           Handler: function(T, W) {
                if(T.startsWith("degli") || T.startsWith("di") || T.startsWith("i") || T.startsWith("dei")) {
                    SelectedMusic = T.substr(T.indexOf(" ") + 1);
                    SelectedArtist = "";
                } else {
                    SelectedMusic = T.substr(0, T.indexOf(" "));
                    SelectedArtist = T.substr(T.lastIndexOf(" ") + 1);
                }

                Jarvis.Log("Media: selected song \"" + SelectedMusic + "\" of " + SelectedArtist);
           }
       },

       {
          Tags: ["voglio", "vorrei", "fammi", "fammela", "fammeli", "avvia", "metti", "apri", "play", "riprendi"],
          Handler: function(T, W) {
              PluginConnector.SendRequest("Media", "PlayMusic", [ ["song", SelectedMusic], ["artist", SelectedArtist] ]);
          }
       },
       
       {
           Tags: ["ferma", "pausa", "stop", "chiudi"],
           Handler: function(T, W) {
               PluginConnector.SendRequest("Media", "StopMusic", []);
           }
       }
    ],
    
    Initialize: function() {
        return;
    }
};

var SelectedMusic = "undefined";
var SelectedArtist = "";

Jarvis.LoadContext(Media);