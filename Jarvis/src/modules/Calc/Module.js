var Calc = {
    Name: "Calc",
    Version: "0.1",
    Help: "Quanto fa 5 + 5? 5 * 10 + 2 fratto 4?",
    Color: "red",
    Dependency: null,
    
    TagContextHandlers: [
        /([-+]?([0-9]|zero|due)*,?([0-9]|zero|due)+\s*([\/\+\-\*x]|fratto|diviso|più)\s*)+([-+]?([0-9]|zero|due)*,?([0-9]|zero|due)+)/
    ],
    
    TagExecHandlers: [
       {
          Tags: [
              /([-+]?([0-9]|zero|due)*,?([0-9]|zero|due)+\s*([\/\+\-\*x]|fratto|diviso|più)\s*)+([-+]?([0-9]|zero|due)*,?([0-9]|zero|due)+)/
          ],
          Handler: function(T, W) {

              T = T
                  .replace(/x/g, "*")
                  .replace(/,/g, ".")
                  .replace(/fratto/g, "/")
                  .replace(/diviso/g, "/")
                  .replace(/zero/g, "0")
                  .replace(/due/g, "2")
                  .replace(/più/g, "+");


              Jarvis.HTTPRequest(
                  "http://geekstribe.altervista.org/ksys/api.php",
                  "GET",
                  [
                      [ "q", "calc" ],
                      [ "e", btoa(T) ]
                  ],
                  function(r) {
                      if(r == "")
                          r = "infinito";
                      r = r.replace(/\./g, ",");

                      Jarvis.Log("Calc: " + T + " = " + r);
                      Jarvis.Talk(r);
                  }
              );
          }
       }
    ],
    
    Initialize: function() {
        return;
    }
};

Jarvis.LoadContext(Calc);