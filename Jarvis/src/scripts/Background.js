
chrome.app.runtime.onLaunched.addListener(function() {
	chrome.app.window.create("src/MainWindow.html", {
		"id" : "MainWindow",
		//"frame" : "none",
		"innerBounds" : {
			"width" : 380,
			"height" : 500
		},
		"maxWidth" : 400,
		"maxHeight" : 500,
		},
		function(wm) {
			wm.contentWindow.onload = function(e) {
				Jarvis.Initialize(wm.contentWindow);
				Jarvis.TalkRandom(RESPONSE_HELLO);
				
				
				Jarvis.Listen(function(e, i) {
					Jarvis.Recognize(e, function(t) {
						Jarvis.TalkRandom(RESPONSE_NOT_UNDERSTOOD);
					});

					var txtInput = Jarvis.GetWindow().document.getElementById("txtInput");
					txtInput.value = i;
				});

				
				Jarvis.OnTalk(function(e) {
					var txtInput = Jarvis.GetWindow().document.getElementById("txtInput");
					txtInput.value = e;
				});
				
				Jarvis.OnContextOpened(function(e) {
					var help = Jarvis.GetWindow().document.getElementById("help");
					help.innerHTML = "Esempi: " + e.Help.substr(0, 60) + "...";	
				});
			};
			
			wm.onClosed.addListener(function() {
				Jarvis.Log("Jarvis: closed.");
				Jarvis.Close();
			});
		}
	);
	
	
});