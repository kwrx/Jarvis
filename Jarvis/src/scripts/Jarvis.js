class Jarvis { 
    static Log(text) {
        console.log(text);
    }

    static GetWindow() {
        return contentWindow;
    }
    
    static Talk(text, pronunciations) {
        if(pronunciations) {
            pronunciations.forEach(function(v) {
                text = text.replace(new RegExp(v.split(":")[0].trim(), "g"), v.split(":")[1].trim());
            });
        }
    
        while(text.search(/\s\s/g) != -1)
            text = text.replace(/\s\s/g, " ");
        text = text.trim();

        TalkChunks = TalkChunks.concat(text.split("."));

        /* Rimuove duplicati */
        var n = 0;
        do {
            n = 0;
            for(var i = 1; i < TalkChunks.length; i++)
                if(TalkChunks[i] == TalkChunks[i - 1])
                    { n++; TalkChunks.splice(i - 1, 1); }
        } while(n > 0);

        
        Utterance.text = TalkChunks[0].trim();
        Utterance.volume = 1.0;
        Utterance.rate = 1.1;
        Utterance.pitch = 1.0;
        
        
        Utterance.onstart = function(e) {
            Jarvis.Log("Jarvis: " + e.currentTarget.text);
            onTalkEvent(e.currentTarget.text);
        }
        
        Utterance.onend = function(e) {
            TalkChunks.splice(0, 1);
            if(!TalkChunks.length) {
                isTalking = false;
                return;
            }
                
            Utterance.text = TalkChunks[0].trim();
            speechSynthesis.speak(Utterance);
        }
        
        if(!isTalking)
            speechSynthesis.speak(Utterance);
            
        isTalking = true;
    }
    
    static TalkRandom(phrases) {
        var i = Math.floor(Math.random() * phrases.length);
        Jarvis.Talk(phrases[i]);
    }
    
    static StopTalking() {
        TalkChunks = new Array();
        
        Utterance.onend = function() {};
        Utterance.onstart = function() {};
        isTalking = false;
        
        speechSynthesis.cancel();
    }
    
    
    static Recognize(text, onFail) {
        if(text == "")
            return;

        Jarvis.Log("Input: " + text.trim());
        
        if(AnswerContextTag != null) {
            onAnswerResult(text.trim());

            var t = AnswerContextTag;
            AnswerContextTag = null;

            return Jarvis.Recognize(t, onFail);
        }


        text = text
            .trim()
            .split(/\be\b|\,|\.|\;|\?|\!/g);


        return (text.filter(function(words, idx, arr) {
            words = words.trim();

            Contexts.every(function(v, i, a) {
                if(!checkContextKeyword(v, words))
                    return true;
                    
                    Jarvis.OpenContext(v);
                    return false;
            });
            
            if(CurrentContext != null)
                if(checkExecKeyword(CurrentContext, words))
                    return executeKeyword(CurrentContext, words);
                        
            if(onFail)
                onFail(text);
                
            return false;
        }).length == text.length);
    }
   
   static StopRecognizing() {
        continueRecognizing = false;
   }

   static AnswerMe(ContextTag, onResult) {
       if(onResult)
          onAnswerResult = onResult;

       AnswerContextTag = ContextTag;
   }
      
    static Listen(onResult) {
        

        Speech.onresult = function(e) {
            var input = ""; var interim = "";
            for(var i = e.resultIndex; i < e.results.length; i++)
                if(e.results[i].isFinal)
                    input += (e.results[i][0].transcript).toLowerCase();
                else
                    interim += (e.results[i][0].transcript).toLowerCase();
              
            onResult(input, interim);
            
        };

        Speech.start(); 
    }
    
    static TagCompare(regex, b) {
       if(typeof regex == "string")
            regex = regex
                .replace(/à/g, "a")
                .replace(/è/g, "e")
                .replace(/ì/g, "i")
                .replace(/ò/g, "o")
                .replace(/ù/g, "u");

        b = b
            .replace(/à/g, "a")
            .replace(/è/g, "e")
            .replace(/ì/g, "i")
            .replace(/ò/g, "o")
            .replace(/ù/g, "u");

        if(typeof regex != "string")
            return regex.test(b);
        
        return new RegExp("\\b" + regex + "\\b").test(b);
    }

    static TagFound(regex, b) {
       if(typeof regex == "string")
            regex = regex
                .replace(/à/g, "a")
                .replace(/è/g, "e")
                .replace(/ì/g, "i")
                .replace(/ò/g, "o")
                .replace(/ù/g, "u");

        b = b
            .replace(/à/g, "a")
            .replace(/è/g, "e")
            .replace(/ì/g, "i")
            .replace(/ò/g, "o")
            .replace(/ù/g, "u");


        if(typeof regex != "string")
            return regex.exec(b);
        
        return new RegExp("\\b" + regex + "\\b").exec(b);
    }
    
    static OpenContext(Context) {
        Jarvis.Log("Jarvis: opening context " + Context.Name);
        if(Context.Dependency != null && CurrentContext != null)
            if(Context.Dependency.Name != CurrentContext.Name)
                return;
                
        CurrentContext = Context;
        onContextOpened(Context);
    }
    
    
    static HTTPRequest(uri, type, params, onResult, onRequest) {
        
        var p = "";
        for(var i = 0; i < params.length; i++) {
            if(i > 0)
                p += "&";
            
            p += params[i][0] + "=" + params[i][1];
        }

        if(type == "GET" && params.length > 0)
            uri += "?" + p;
        

        var ajax = new XMLHttpRequest();
        ajax.open(type, uri, true);
        
        if(onRequest)
            onRequest(ajax);
        
        ajax.send(type == "POST" ? p : null);
        
        ajax.onreadystatechange = function() {
            if(ajax.readyState == 4)
                if(ajax.status == 0 || ajax.status == 200)
                    onResult(ajax.responseText);
        }
    }
    
    
    static LoadFile(path) {
        return Jarvis.HTTPRequest(path, "GET");
    }
    
    static LoadContext(Context) {
        Contexts.push(Context);
    }
    
    static Close() {
        Speech.stop();
    }
    
    static Initialize(window) {
        if(window)
            contentWindow = window;

        speechSynthesis.onvoiceschanged = function(e) {
            Utterance.voice = speechSynthesis.getVoices().filter(function(v) {
                return v.name === "Google italiano"; 
            })[0];
        }
        
        Contexts.forEach(function(v) {
            Jarvis.Log("Loading Context: " + v.Name);
            v.Initialize();
        });
        
        Jarvis.Log("Jarvis: initialized!");
    }
    
    
    /* Events */
    static OnTalk(e) {
        onTalkEvent = e;
    }
    
    static OnContextOpened(e) {
        onContextOpened = e;
    }
}


var RESPONSE_OK =
    ["Va bene", "OK"];
var RESPONSE_NOT_UNDERSTOOD =
    ["Non ho capito, puoi ripetere perfavore?", "Non capisco"];
var RESPONSE_HELLO =
    ["Ciao!", "Salve!"];

var TAGS_DATE = 
    [ "oggi", "domani", "dopodomani", "domenica", "lunedì", "martedì", "mercoledì", "giovedì", "venerdì", "sabato", /\b(\d+\s+\w+\s+\d+|\d+\s+\w+|\d+)\b/];

var TAGS_TIME = 
    [ /\b\d{1,2}:\d{1,2}\b/ ];



var onTalkEvent = function() {};
var onContextOpened = function(e) {};
var onAnswerResult = function(e) {};

var Contexts = new Array();
var TalkChunks = new Array();
var isTalking = false;
var CurrentContext = null;
var AnswerContextTag = null;
var Utterance = new SpeechSynthesisUtterance();
var contentWindow = null;

/* Speech Engine Init */
var Speech = new webkitSpeechRecognition();
Speech.lang = "it";
Speech.continuous = true;
Speech.interimResults = true;

Speech.onend = function(e) {
    Speech.stop();
    Speech.start();
}

Speech.onerror = function(e) {
    switch(e.error) {
        case "no-speech":
        case "aborted":
            break;
        default:
            Jarvis.Log(e);
    }  
}

/***********************/
 

var checkContextKeyword = function(Context, Words) {
    for(var i = 0; i < Context.TagContextHandlers.length; i++)
            if(Jarvis.TagCompare(Context.TagContextHandlers[i], Words)) 
                return true;
                
    return false;
}

var checkExecKeyword = function(Context, Words) {
     for(var i = 0; i < Context.TagExecHandlers.length; i++)
        for(var j = 0; j < Context.TagExecHandlers[i].Tags.length; j++)
                if(Jarvis.TagCompare(Context.TagExecHandlers[i].Tags[j], Words))
                    return true;
                    
     return false;
}

var executeKeyword = function(Context, Words) {
    continueRecognizing = true;
    
     for(var i = 0; i < Context.TagExecHandlers.length; i++)
        for(var j = 0; j < Context.TagExecHandlers[i].Tags.length; j++)
            if(Jarvis.TagCompare(Context.TagExecHandlers[i].Tags[j], Words))
                if(continueRecognizing)
                    Context.TagExecHandlers[i].Handler(Jarvis.TagFound(Context.TagExecHandlers[i].Tags[j], Words)[0], Words);

    return true;
}